package com.listss.listss;

import android.app.Application;

import com.bumptech.glide.request.target.ViewTarget;

/**
 * Created by ines_ on 26/02/2018.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ViewTarget.setTagId(R.id.glide_tag);
    }
}
