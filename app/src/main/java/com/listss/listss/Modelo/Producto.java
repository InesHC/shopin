package com.listss.listss.Modelo;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by ines_ on 01/11/2017.
 */
@IgnoreExtraProperties
public class Producto {
    private String nombre;
    @Exclude
    private String id;
    private int cantidad=1;
    private double precio=0;
    private boolean comprado=false;

    public Producto(String nombre, String id, int cantidad, double precio, boolean comprado) {
        this.nombre = nombre;
        this.id = id;
        this.cantidad = cantidad;
        this.precio = precio;
        this.comprado = comprado;
    }
    public Producto(){}

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public boolean isComprado() {
        return comprado;
    }

    public void setComprado(boolean comprado) {
        this.comprado = comprado;
    }

    @Override
    public String toString() {
        return "id: "+getId()+" nombre: "+getNombre()+" cantidad: "+getCantidad()+" precio: "+getPrecio()+" comprado: "+isComprado();
    }
}
