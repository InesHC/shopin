package com.listss.listss.Modelo;

import android.support.annotation.NonNull;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Created by ines_ on 01/11/2017.
 */
@IgnoreExtraProperties
public class Lista implements Comparable<Lista> {
    @Exclude
    private String id = "";

    //private List<Producto> productos=new ArrayList<>();
    private String nombre;
    private HashMap<String, Boolean> participantes = new HashMap<>();
    private String imagen = null;

    public Lista() {
    }

    public Lista(String nombre) {
        this.nombre = nombre;
    }

    public Lista(String nombre, HashMap<String, Boolean> participantes) {
        this.nombre = nombre;
        this.participantes = participantes;
    }

    public Lista(String id, String nombre, HashMap<String, Boolean> participantes) {
        this.nombre = nombre;
        this.id = id;
        this.participantes = participantes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /*public List<Producto> getProductos() {
        return productos;
    }
    public void addProducto(Producto producto){
        this.productos.add(producto);
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }*/

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public HashMap<String, Boolean> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(HashMap<String, Boolean> participantes) {
        this.participantes = participantes;
    }

    public void addParticipante(String usuario) {
        getParticipantes().put(usuario, true);
    }

    public int numParticipantes() {
        return getParticipantes().size();
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return "id: " + getId() + " nombre: " + getNombre();
    }

    @Override
    public boolean equals(Object obj) {
        return ((Lista) obj).getId().equals(getId());
    }

    @Override
    public int compareTo(@NonNull Lista o) {
        return getId().compareTo(o.getId());
    }
}
