package com.listss.listss.Modelo;

import android.support.v4.view.animation.LinearOutSlowInInterpolator;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ines_ on 01/11/2017.
 */

@IgnoreExtraProperties
public class Usuario {
    private String id;
    private String nombre;
    private HashMap<String,Boolean> listas=new HashMap<>();

    public Usuario(){}

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public HashMap<String,Boolean> getListas() {
        return listas;
    }

    public void setListas(HashMap<String,Boolean> listas) {
        this.listas = listas;
    }
}
