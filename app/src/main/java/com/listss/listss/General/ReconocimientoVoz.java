package com.listss.listss.General;

import android.app.Activity;

import android.app.Fragment;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v4.app.DialogFragment;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by ines_ on 06/11/2017.
 */

public class ReconocimientoVoz {
    private static final int VOICE_RECOGNITION_REQUEST_CODE = 1;

    /**
     * Iniciar reconocimiento de voz
     * @param fragmento
     */
    public static void startVoiceRecognitionActivity(DialogFragment fragmento) {
        // Definición del intent para realizar en análisis del mensaje
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        // Indicamos el modelo de lenguaje para el intent
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        // Definimos el mensaje que aparecerá
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Di algo");
        // Lanzamos la actividad esperando resultados
        fragmento.startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    }

    /**
     * Función que se debe utilizar en un onActivityResult para recoger el resultado del reconocimiento de voz
     * @param requestCode
     * @param resultCode
     * @param data
     * @param fragmento
     * @return
     */
    public static String resultadoRV(int requestCode, int resultCode, Intent data, DialogFragment fragmento){
        //Si el reconocimiento a sido bueno
        if(requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            //El intent nos envia un ArrayList aunque en este caso solo
            //utilizaremos la pos.0
            ArrayList<String> matches = data.getStringArrayListExtra
                    (RecognizerIntent.EXTRA_RESULTS);
            //Devuelvo el texto reconocido
            return General.ucFirst(matches.get(0));
        }
        return null;

    }
}
