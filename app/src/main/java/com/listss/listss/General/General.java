package com.listss.listss.General;


import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import com.listss.listss.Logica.Activity.InicioActivity;
import com.listss.listss.R;

/**
 * Created by ines_ on 31/10/2017.
 */

public class General {
    public static void hideActivityTitle(AppCompatActivity activity) {
        activity.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    public static void cerrarApp(boolean confirmar, Activity actividad) {
        if(confirmar){ //Mostrar alerta para confirmar

            actividad.finishAffinity();
        }
        else{
            System.exit(0);
        }
    }

    public static void setActividad(Activity actividad, Class actividadnueva) {
        actividad.startActivity(new Intent(actividad, actividadnueva));
    }

    public static boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    public static boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 5;
    }

    public static void setFragment(Fragment fragment, Activity actividad, boolean permiteVolver) {
        FragmentTransaction ft = ((AppCompatActivity) actividad).getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content, fragment, "fragmento");
        if (permiteVolver) {
            ft.addToBackStack(null);
        }
        ft.commit();
    }

    public static void backButton(final Fragment fragment, boolean mostrar) {
        ActionBar ab = ((AppCompatActivity) fragment.getActivity()).getSupportActionBar();
        // To keep states of ActionBar and ActionBarDrawerToggle synchronized,
        // when you enable on one, you disable on the other.
        // And as you may notice, the order for this operation is disable first, then enable - VERY VERY IMPORTANT.
        if (mostrar) {
            // Remove hamburger
            InicioActivity.toggleInicio.setDrawerIndicatorEnabled(false);
            // Show back button
            ab.setDisplayHomeAsUpEnabled(true);
            // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
            // clicks are disabled i.e. the UP button will not work.
            // We need to add a listener, as in below, so DrawerToggle will forward
            // click events to this listener.
            //if(!InicioActivity.mToolBarNavigationListenerIsRegistered) {
            InicioActivity.toggleInicio.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Doesn't have to be onBackPressed
                    fragment.getActivity().onBackPressed();
                }
            });
            InicioActivity.mToolBarNavigationListenerIsRegistered = true;
            //}
        } else {
            // Remove back button
            ab.setDisplayHomeAsUpEnabled(false);
            // Show hamburger
            InicioActivity.toggleInicio.setDrawerIndicatorEnabled(true);
            // Remove the/any drawer toggle listener
            InicioActivity.toggleInicio.setToolbarNavigationClickListener(null);
            InicioActivity.mToolBarNavigationListenerIsRegistered = false;
        }
    }

    public static String ucFirst(String str) {
        if (str.isEmpty()) {
            return str;
        } else {
            return Character.toUpperCase(str.charAt(0)) + str.substring(1);
        }
    }

    /**
     * Crea una clave aleatoria
     *
     * @return
     */
    public static String nuevaClave() {
        char[] elementos = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a',
                'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't',
                'u', 'v', 'w', 'x', 'y', 'z'};
        char[] conjunto = new char[8];
        String pass;
        for (int i = 0; i < 8; i++) {
            int el = (int) (Math.random() * 37);
            conjunto[i] = (char) elementos[el];
        }
        return pass = new String(conjunto);
    }

    public static Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
}
