package com.listss.listss.Logica.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.listss.listss.General.General;
import com.listss.listss.Modelo.Lista;
import com.listss.listss.Modelo.Producto;

import com.listss.listss.Persistencia.ListasDb;
import com.listss.listss.Persistencia.ProductosDb;
import com.listss.listss.Persistencia.UsuariosDb;

import com.listss.listss.Logica.Dialog.CambioListaDialog;
import com.listss.listss.Logica.Dialog.CambioProductoDialog;

import com.listss.listss.Logica.Dialog.NuevoProductoDialog;
import com.listss.listss.Logica.SwipeRecycler;

import com.listss.listss.Logica.ViewHolder.ProductoViewHolder;
import com.listss.listss.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ProductoFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private FirebaseRecyclerAdapter mAdapter;
    public static Lista lista = new Lista();
    public static String idLista = "";
    //Atributos para borrar un producto
    private static List<Integer> itemsPendingRemoval = new ArrayList<Integer>();
    private static final int PENDING_REMOVAL_TIMEOUT = 3000; // 3sec
    private static HashMap<Integer, Thread> removing = new HashMap<>();
    private static HashMap<Integer, String> posicionProducto = new HashMap<>();
    public static boolean isRunning = false;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ProductoFragment() {
    }

    public static ProductoFragment newInstance(int columnCount, Lista list) {
        ProductoFragment fragment = new ProductoFragment();
        if (list != null) {
            lista = list;
            idLista = lista.getId();
        } else {
            lista.setId(idLista);
        }
        System.out.println("ProductoFragement: " + idLista);
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_eliminar:
                ListasDb.eliminarParticipante(lista, UsuariosDb.idUsuario());
                //Vuelve al listado de las listas del usuario
                getFragmentManager().popBackStack();
                return true;
            case R.id.action_editar:
                //Abre el diálogo para cambiar los datos de la lista
                CambioListaDialog dialog = new CambioListaDialog();
                dialog.setLista(lista);
                dialog.show(getActivity().getSupportFragmentManager(), "CambioListaDialog");
                return true;
            case R.id.action_compartir:
                //Va al fragmento con el qr de la lista
                General.setFragment(GenQRFragment.newInstance(lista), getActivity(), true);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_producto_list, container, false);
        final View vista = view.findViewById(R.id.list);
        FloatingActionButton addProd = view.findViewById(R.id.add_product);
        addProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NuevoProductoDialog dialog = new NuevoProductoDialog();
                dialog.setLista(idLista);
                dialog.show(getActivity().getSupportFragmentManager(), "NuevoProductoDialog");
            }
        });
        ListasDb.getLista(idLista).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    lista = dataSnapshot.getValue(Lista.class);
                    lista.setId(idLista);
                    Activity activity = getActivity();
                    if (activity != null) {
                        activity.setTitle(lista.getNombre());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        // Set the adapter
        if (vista instanceof RecyclerView) {
            Context context = vista.getContext();
            RecyclerView recyclerView = (RecyclerView) vista;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            //Asigna el adaptador de Firebase para llenar el RecyclerView
            FirebaseRecyclerOptions<Producto> options = new FirebaseRecyclerOptions.Builder<Producto>().setQuery(ProductosDb.getProductos(lista.getId()), Producto.class).build();
            mAdapter = new FirebaseRecyclerAdapter<Producto, ProductoViewHolder>(options) {
                @Override
                public ProductoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                    // Create a new instance of the ViewHolder, in this case we are using a custom
                    // layout called R.layout.message for each item
                    View view = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.fragment_producto, parent, false);
                    return new ProductoViewHolder(view);
                }

                @Override
                protected void onBindViewHolder(ProductoViewHolder holder, final int position, final Producto producto) {
                    producto.setId(this.getRef(position).getKey());
                    posicionProducto.put(position, producto.getId());
                    boolean pendiente = itemsPendingRemoval.contains(position);
                    holder.setVista(producto, pendiente, lista);
                    if (pendiente) {
                        holder.tvUndo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Interrumpe la eliminación del producto
                                removing.get(position).interrupt();
                                //Cambia la vista de eliminación
                                quitarPendiente(position);
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        holder.mView.setOnClickListener(new View.OnClickListener() {//Cambio de datos de un producto
                            @Override
                            public void onClick(View v) {
                                CambioProductoDialog dialog = new CambioProductoDialog();
                                dialog.setLista(idLista);
                                dialog.setProducto(producto);
                                dialog.show(getActivity().getSupportFragmentManager(), "CambioProductoDialog");
                            }
                        });
                    }
                }
            };

            recyclerView.setAdapter(mAdapter);
            setSwipeForRecyclerView(recyclerView);
        }
        return view;
    }

    private void setSwipeForRecyclerView(RecyclerView view) {

        SwipeRecycler swipeHelper = new SwipeRecycler(0, ItemTouchHelper.LEFT, getActivity()) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                final int swipedPosition = viewHolder.getAdapterPosition();
                //Añadir como pendiente de eliminar
                itemsPendingRemoval.add(swipedPosition);
                mAdapter.notifyDataSetChanged();


                Thread th = new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(PENDING_REMOVAL_TIMEOUT);
                            quitarPendiente(swipedPosition);
                            ProductosDb.eliminarProducto(lista.getId(), posicionProducto.get(swipedPosition));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                removing.put(swipedPosition, th);
                th.start();
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                if (itemsPendingRemoval.contains(position)) {
                    return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }
        };

        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(swipeHelper);
        mItemTouchHelper.attachToRecyclerView(view);

        //set swipe background-Color
        swipeHelper.setLeftcolorCode(ContextCompat.getColor(getActivity(), R.color.colorPrimary));

    }

    private void quitarPendiente(int position) {
        itemsPendingRemoval.remove((Integer) position);
        removing.remove(position);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        isRunning = true;
        getActivity().setTitle(lista.getNombre());
        General.backButton(this, true);
        if (mAdapter != null) {
            mAdapter.startListening();
        }
        if (lista.getNombre() == null) {
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        isRunning = false;
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Producto item);
    }
}
