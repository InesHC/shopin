package com.listss.listss.Logica.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.listss.listss.Modelo.Producto;
import com.listss.listss.Persistencia.ProductosDb;
import com.listss.listss.R;

/**
 * Created by ines_ on 02/11/2017.
 */

public class CambioProductoDialog extends NuevoProductoDialog {
    private Producto producto;

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Get the layout inflater
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View vista = inflater.inflate(R.layout.nuevo_producto, null);
        nombre = vista.findViewById(R.id.etNombre);
        cantidad = vista.findViewById(R.id.etCantidad);
        precio = vista.findViewById(R.id.etPrecio);
        //Cargar datos de producto
        nombre.setText(producto.getNombre());
        cantidad.setText(String.valueOf(producto.getCantidad()));
        precio.setText(String.valueOf(producto.getPrecio()));

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final AlertDialog ad = builder.setTitle("Cambio producto")
                .setView(vista)
                .setPositiveButton("Guardar", null) //Set to null. We override the onclick
                .setNegativeButton(R.string.cancel, null)
                .setCancelable(false)
                .create();
        ad.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = ad.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Crear lista
                        Producto p = datosValidos(nombre, cantidad, precio);
                        if (p != null) {
                            ProductosDb.setProducto(idLista, producto.getId(), p);
                            ad.dismiss();
                        }
                    }
                });
            }
        });
        return ad;
    }
}
