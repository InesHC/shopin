package com.listss.listss.Logica.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.listss.listss.General.General;
import com.listss.listss.Logica.Dialog.NuevaListaDialog;
import com.listss.listss.Logica.ViewHolder.ListaViewHolder;
import com.listss.listss.Modelo.Lista;
import com.listss.listss.Persistencia.UsuariosDb;
import com.listss.listss.R;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ListaFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private String TAGLOG = "BD listas";
    private RecyclerView recyclerView;
    public static FirebaseRecyclerAdapter mAdapter;
    public static ArrayList<Lista> itemsSelect = new ArrayList<>();
    public static ActionMode unoAction;
    ListaFragment fragmento = this;
    private FloatingActionsMenu fab;
    public static boolean isRunning;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListaFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ListaFragment newInstance(int columnCount) {
        ListaFragment fragment = new ListaFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Listas");
        View view = inflater.inflate(R.layout.fragment_lista_list, container, false);
        View vista = view.findViewById(R.id.list);
        fab = view.findViewById(R.id.menu_fab);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                System.out.println("Lista fragment: touch!");//No funciona!
                fab.collapse();
                return true;
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Lista fragment: click!");//No funciona!
                fab.collapse();
            }
        });

        FloatingActionButton addList = view.findViewById(R.id.add_list);
        addList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NuevaListaDialog dialog = new NuevaListaDialog();
                dialog.show(getActivity().getSupportFragmentManager(), "NuevaListaDialog");
            }
        });
        FloatingActionButton scanList = view.findViewById(R.id.scan_list);
        scanList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                General.setFragment(ScanQRFragment.newInstance(), getActivity(), true);
            }
        });
        /*android.support.design.widget.FloatingActionButton scanList = view.findViewById(R.id.scan_list);
        scanList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                General.setFragment(ScanQRFragment.newInstance(),getActivity(),true);
            }
        });
        android.support.design.widget.FloatingActionButton addList =  view.findViewById(R.id.add_list);
        addList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NuevaListaDialog dialog = new NuevaListaDialog();
                dialog.show(getActivity().getSupportFragmentManager(), "NuevaListaDialog");
            }
        });*/
        // Set the adapter
        if (vista instanceof RecyclerView) {
            Context context = vista.getContext();
            recyclerView = (RecyclerView) vista;
            recyclerView.setHasFixedSize(true);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            FirebaseRecyclerOptions<Boolean> options = new FirebaseRecyclerOptions.Builder<Boolean>().setQuery(UsuariosDb.listasUsuario(), Boolean.class).build();
            mAdapter = new FirebaseRecyclerAdapter<Boolean, ListaViewHolder>(options) {
                @Override
                public ListaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                    // Create a new instance of the ViewHolder, in this case we are using a custom
                    // layout called R.layout.message for each item
                    View view = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.fragment_lista, parent, false);

                    return new ListaViewHolder(view);
                }

                @Override
                protected void onBindViewHolder(ListaViewHolder holder, int position, Boolean model) {
                    //Recoge la clave de la lista
                    final String idLista = this.getRef(position).getKey();
                    //Cambia la vista
                    holder.setVista(idLista, fragmento, position, mAdapter, recyclerView);
                }
            };
            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle("Listas");
        //Si está en la lista, vacía la cola de fragments para volver atrás.
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        General.backButton(this, false);
        isRunning = true;
        mAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        isRunning = false;
        mAdapter.stopListening();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Lista item);
    }
}
