package com.listss.listss.Logica.ViewHolder;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.listss.listss.General.General;
import com.listss.listss.Logica.Dialog.CambioListaDialog;
import com.listss.listss.Logica.Fragment.GenQRFragment;
import com.listss.listss.Logica.Fragment.ListaFragment;
import com.listss.listss.Logica.Fragment.ProductoFragment;
import com.listss.listss.Modelo.Lista;
import com.listss.listss.Persistencia.ImagenesFirebase;
import com.listss.listss.Persistencia.ListasDb;
import com.listss.listss.Persistencia.UsuariosDb;
import com.listss.listss.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ines_ on 02/11/2017.
 */

public class ListaViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    private final TextView tvNombre;
    private final TextView tvParticipantes;
    private FragmentActivity actividad;
    private Lista lista;
    private ArrayList<Lista> seleccionadas = ListaFragment.itemsSelect;
    private FirebaseRecyclerAdapter adaptador;
    private RecyclerView vista;
    private static SparseArray<Lista> datosVista = new SparseArray<>();
    public final CircleImageView imagen;

    public ListaViewHolder(View view) {
        super(view);
        mView = view;
        tvNombre = view.findViewById(R.id.content);
        tvParticipantes = view.findViewById(R.id.participantes);
        imagen = view.findViewById(R.id.imagenLista);
    }

    public void setVista(final String idLista, final Fragment fragmento, final int position, FirebaseRecyclerAdapter adapter, RecyclerView vista) {
        this.actividad = fragmento.getActivity();
        this.vista = vista;
        adaptador = adapter;
        //Carga los datos de la lista
        ListasDb.getLista(idLista).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lista = dataSnapshot.getValue(Lista.class);
                if (lista != null) {
                    //if(!lista.getImagen().equalsIgnoreCase("cambiada")){
                    lista.setId(idLista);
                    datosVista.put(position, lista);
                    if (!actividad.isDestroyed()) {
                        ImagenesFirebase.cargarImagenLista(imagen, lista);
                    }
                    tvNombre.setText(lista.getNombre());
                    int participantes = lista.getParticipantes().size();
                    tvParticipantes.setText(participantes + (participantes == 1 ? " participante" : " participantes"));

                    mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {//Muestra el contenido de la lista
                            seleccionadas = ListaFragment.itemsSelect;
                            if (seleccionadas.size() > 0) {//Gestiona selección
                                if (ListaFragment.unoAction == null) {
                                    iniciarActionMode();
                                }
                                if (seleccionadas.contains(lista)) {//Quita la selección
                                    seleccionadas.remove(lista);
                                    v.setBackgroundColor(Color.WHITE);
                                    if (seleccionadas.size() == 0) {//Reseleccionado el único item
                                        //Quitar todas las opciones de barra de acción
                                        ListaFragment.unoAction.finish();
                                        fragmento.setHasOptionsMenu(true);
                                    } else if (seleccionadas.size() == 1) {//Quitar selección de un item, pero deja el resto
                                        //Mostrar opciones para 1 lista (editar y generar qr)
                                        //unoAction.invalidate();

                                        seleccion(true);
                                    }
                                } else {//Añadir selección
                                    seleccionadas.add(lista);
                                    v.setBackgroundColor(actividad.getResources().getColor(R.color.colorPrimaryDark));
                                    seleccion(false);
                                }
                            } else {
                                ProductoFragment pf = ProductoFragment.newInstance(0, lista);
                                General.setFragment(pf, actividad, true);
                            }
                        }
                    });
                    mView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {//Permite eliminar, editar y generar el código QR de la lista
                            if (seleccionadas.size() == 0) {
                                seleccionadas.add(lista);
                                v.setBackgroundColor(actividad.getResources().getColor(R.color.colorPrimaryDark));
                                //Añadir opciones de barra de acción
                                iniciarActionMode();
                                fragmento.setHasOptionsMenu(false);
                                return true;
                            } else {
                                return false;
                            }
                        }
                    });
                    //}
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("ERROR CARGA LISTA ", idLista + ": " + databaseError.getMessage());
            }
        });
    }

    private void seleccion(boolean uno) {
        Menu m = ListaFragment.unoAction.getMenu();
        m.getItem(0).setVisible(uno);
        m.getItem(1).setVisible(uno);
    }

    private void iniciarActionMode() {
        ListaFragment.unoAction = ((AppCompatActivity) actividad).startSupportActionMode(mUnoModeCallback);
    }

    private void quitarSelecciones() {
        seleccionadas.clear();
        vista.setAdapter(adaptador);//Reinicia la vista
    }

    private ActionMode.Callback mUnoModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.select_one_list, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            switch (item.getItemId()) {
                case R.id.action_eliminar:
                    for (Lista l : seleccionadas) {
                        ListasDb.eliminarParticipante(l, UsuariosDb.idUsuario());
                    }
                    ListaFragment.unoAction.finish();
                    return true;
                case R.id.action_editar:
                    CambioListaDialog dialog = new CambioListaDialog();
                    if (seleccionadas.size() > 0) {
                        dialog.setPosicion(datosVista.indexOfValue(seleccionadas.get(0)));
                        dialog.setLista(seleccionadas.get(0));
                        dialog.show(actividad.getSupportFragmentManager(), "CambioListaDialog");
                    }
                    ListaFragment.unoAction.finish();
                    //Editar lista
                    return true;
                case R.id.action_compartir:
                    General.setFragment(GenQRFragment.newInstance(seleccionadas.get(0)), actividad, true);
                    ListaFragment.unoAction.finish();
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            quitarSelecciones();
            //ListaFragment.unoAction = null;
        }
    };
}
