package com.listss.listss.Logica.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.listss.listss.Modelo.Lista;
import com.listss.listss.Modelo.Producto;
import com.listss.listss.Persistencia.ProductosDb;
import com.listss.listss.R;

/**
 * Created by ines_ on 02/11/2017.
 */

public class ProductoViewHolder extends RecyclerView.ViewHolder{
    public final View mView;
    private final TextView tvNombre;
    private final TextView tvPrecio;
    private final TextView tvCantidad;
    public final CheckBox cbComprado;
    private final View vDatos;
    public final TextView tvUndo;
    public Producto item;

    public ProductoViewHolder(View view) {
        super(view);
        mView = view;
        //mIdView = (TextView) view.findViewById(R.id.id);
        tvNombre = (TextView) view.findViewById(R.id.nombreProducto);
        tvPrecio=(TextView)view.findViewById(R.id.precioProducto);
        tvCantidad=(TextView)view.findViewById(R.id.cantidadProducto);
        cbComprado=(CheckBox)view.findViewById(R.id.compradoProducto);
        vDatos=view.findViewById(R.id.view_datos);
        tvUndo=(TextView)view.findViewById(R.id.eliminar_deshacer);
    }
    public void setVista(final Producto producto, boolean eliminado, final Lista lista){
        if(eliminado){
            vDatos.setVisibility(View.GONE);
        }
        else {
            vDatos.setVisibility(View.VISIBLE);
            item = producto;
            tvNombre.setText(producto.getNombre());
            String textoPrecio = producto.getPrecio() + " €";
            tvPrecio.setText(textoPrecio);
            tvCantidad.setText(String.valueOf(producto.getCantidad()));
            //Previene situaciones indeseadas
            cbComprado.setOnCheckedChangeListener(null);
            cbComprado.setChecked(producto.isComprado());
            //Marca la compra o no de un producto
            cbComprado.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.d("Checked","producto: "+producto.getId()+" "+isChecked);
                    ProductosDb.comprar(lista,producto.getId(),isChecked, cbComprado.getContext());

                }
            });
            Log.d("Cargar datos ",item.getId()+": "+cbComprado.isChecked());
        }

    }
}
