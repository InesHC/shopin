package com.listss.listss.Logica.Activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.listss.listss.General.General;
import com.listss.listss.Logica.Fragment.GenQRFragment;
import com.listss.listss.Logica.Fragment.ListaFragment;
import com.listss.listss.Logica.Fragment.ProductoFragment;
import com.listss.listss.Logica.Fragment.ScanQRFragment;
import com.listss.listss.Modelo.Lista;
import com.listss.listss.Modelo.Producto;
import com.listss.listss.Persistencia.BaseDb;
import com.listss.listss.Persistencia.LogFirebase;
import com.listss.listss.Persistencia.UsuariosDb;
import com.listss.listss.R;

import java.util.concurrent.Callable;


public class InicioActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ListaFragment.OnListFragmentInteractionListener,
        ProductoFragment.OnListFragmentInteractionListener, ScanQRFragment.OnFragmentInteractionListener, GenQRFragment.OnFragmentInteractionListener {
    public static ActionBarDrawerToggle toggleInicio;
    public static boolean mToolBarNavigationListenerIsRegistered = false;
    private AppCompatActivity actividad = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        BaseDb.iniciarConfiguracion();
        super.onCreate(savedInstanceState);
        String idLista = getIntent().getStringExtra("idLista");
        if (UsuariosDb.estadoUsuario() == null) {
            General.setActividad(this, LoginActivity.class);
        } else if (idLista == null) {
            setContentView(R.layout.activity_main);
            //Inicia el fragment
            cargarFragmento(ListaFragment.newInstance(0), false);
        } else {
            //Abre la lista indicada por su identificador
            ProductoFragment.idLista = idLista;
            cargarFragmento(ProductoFragment.newInstance(0, null), false);
        }
    }

    private void cargarFragmento(Fragment fragmento, boolean permiteVolver) {
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        toggleInicio = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggleInicio);
        toggleInicio.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //Inicia el fragment
        General.setFragment(fragmento, this, permiteVolver);
    }

    @Override
    public void onBackPressed() {
        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/
        //Comprueba si está en ListaFragment
        if (ListaFragment.isRunning) {
            super.onBackPressed();//Comprobar si viene de iniciar seión?
            General.cerrarApp(true, this);
        }
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_logout://Cierra la sesión
                LogFirebase.cerrarSesion(actividad, new Callable() {
                    @Override
                    public Object call() throws Exception {
                        General.setActividad(actividad, LoginActivity.class);
                        return null;
                    }
                });
                break;
            case R.id.nav_faq:
                break;
            case R.id.nav_conditions:
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onListFragmentInteraction(Lista item) {

    }

    @Override
    public void onListFragmentInteraction(Producto item) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
