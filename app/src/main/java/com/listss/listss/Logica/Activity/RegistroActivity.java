package com.listss.listss.Logica.Activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.listss.listss.General.General;
import com.listss.listss.Persistencia.LogFirebase;
import com.listss.listss.R;


public class RegistroActivity extends InicioUsuarioActivity {
    private EditText mPasswordView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        General.hideActivityTitle(this);
        setContentView(R.layout.activity_registro);
        super.onCreate(savedInstanceState);

        mPasswordView2 = findViewById(R.id.password2);
        mPasswordView2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin(null);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected View attemptLogin(View focusView) {
        focusView = super.attemptLogin(focusView);
        // Reset errors.
        mPasswordView2.setError(null);
        if (focusView == null) {
            // Store values at the time of the login attempt.
            String password2 = mPasswordView2.getText().toString();

            // Check for a valid password, if the user entered one.
            if (TextUtils.isEmpty(password2)) {
                mPasswordView2.setError(getString(R.string.error_field_required));
                focusView = mPasswordView2;
            } else if (!password2.equals(mPasswordView.getText().toString())) {
                mPasswordView2.setError(getString(R.string.error_pwd2));
                focusView = mPasswordView2;
            }
        }

        if (focusView != null) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            super.showProgress(true);

        }
        return focusView;
    }

    public void registrar(View view) {
        if (attemptLogin(null) == null) {
            EditText etp2 = findViewById(R.id.password2);
            String pwd2 = etp2.getText().toString();
            EditText etp = findViewById(R.id.password);
            String pwd = etp.getText().toString();
            AutoCompleteTextView etc = findViewById(R.id.email);
            String email = etc.getText().toString();
            if (pwd.equals(pwd2)) {
                //Log.d("Registro",email+":"+pwd);
                LogFirebase.crearUsuario(email, pwd, this);
                super.showProgress(false);
            }
        }
    }

    public void volverAInicio(View view) {
        General.setActividad(this, LoginActivity.class);
    }

}
