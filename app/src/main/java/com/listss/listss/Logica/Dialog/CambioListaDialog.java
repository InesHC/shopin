package com.listss.listss.Logica.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.listss.listss.Logica.Fragment.ListaFragment;
import com.listss.listss.Logica.Fragment.ProductoFragment;
import com.listss.listss.Modelo.Lista;
import com.listss.listss.Persistencia.ImagenesFirebase;
import com.listss.listss.Persistencia.ListasDb;
import com.listss.listss.R;

import java.util.concurrent.Callable;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ines_ on 02/11/2017.
 */

public class CambioListaDialog extends NuevaListaDialog {
    private Lista lista = ProductoFragment.lista;
    private int posicion;

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public void setLista(Lista lista) {
        this.lista = lista;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View vista = cargarVista();
        final AlertDialog ad = builder.setTitle("Cambio lista")
                .setView(vista)
                .setPositiveButton("Guardar", null) //Set to null. We override the onclick
                .setNegativeButton(R.string.cancel, null)
                .setCancelable(false)
                .create();
        ad.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                //Cargar datos de la lista
                final CircleImageView imagenLista = vista.findViewById(R.id.img_lista);
                ImagenesFirebase.cargarImagenLista(imagenLista, lista);
                final EditText etn = vista.findViewById(R.id.etNombreLista);
                etn.setText(lista.getNombre());
                Button b = ad.getButton(AlertDialog.BUTTON_POSITIVE);
                //Carga datos
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Cambiar lista
                        String nombre = etn.getText().toString();
                        if (camposValidos(etn, nombre)) {
                            lista.setNombre(nombre);
                            ListasDb.setDatosLista(lista, imagenLista, new Callable() {
                                @Override
                                public Object call() throws Exception {
                                    if (posicion >= 0) {
                                        ListaFragment.mAdapter.notifyDataSetChanged();
                                    }
                                    return null;
                                }
                            });
                            if (!ListaFragment.isRunning) {
                                getActivity().setTitle(nombre);
                            }
                            ad.dismiss();
                        }
                    }
                });
            }
        });
        return ad;
    }
}
