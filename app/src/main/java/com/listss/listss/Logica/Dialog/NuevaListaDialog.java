package com.listss.listss.Logica.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.listss.listss.General.Imagenes;
import com.listss.listss.Persistencia.ListasDb;
import com.listss.listss.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ines_ on 02/11/2017.
 */

public class NuevaListaDialog extends DialogFragment {
    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    protected static final int PICK_IMAGE_ID = 234; // the number doesn't matter
    protected CircleImageView imagenLista;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View vista = cargarVista();
        final AlertDialog ad = builder.setTitle("Nueva lista")
                .setView(vista)
                .setPositiveButton("Crear", null) //Set to null. We override the onclick
                .setNegativeButton(R.string.cancel, null)
                .setCancelable(false)
                .create();
        ad.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = ad.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Crear lista
                        EditText etn = vista.findViewById(R.id.etNombreLista);
                        String nombre = etn.getText().toString();

                        if (camposValidos(etn, nombre)) {
                            ListasDb.nuevaLista(nombre, imagenLista);
                            ad.dismiss();
                        }
                    }
                });
            }
        });
        return ad;
    }

    protected View cargarVista() {
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View v = inflater.inflate(R.layout.nueva_lista, null);
        imagenLista = v.findViewById(R.id.img_lista);
        imagenLista.setTag(Imagenes.imagenDefecto);
        CircleImageView camera = v.findViewById(R.id.btSelectImage);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = Imagenes.getPickImageIntent(getContext());
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
            }
        });
        return v;
    }

    protected boolean camposValidos(EditText etNombre, String nombre) {
        if (nombre.isEmpty()) {
            etNombre.setError("El campo es requerido");
        } else if (nombre.length() < 3 || nombre.length() > 30) {
            etNombre.setError("El nombre debe tener una longitud entre 3 y 30 caracteres");
        } else {
            etNombre.setError(null);
            return true;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_IMAGE_ID:
                Bitmap bitmap = Imagenes.getImageFromResult(getContext(), resultCode, data);
                imagenLista.setImageBitmap(bitmap);
                imagenLista.setTag("Imagen nueva");
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
}
