package com.listss.listss.Logica.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.listss.listss.General.ReconocimientoVoz;
import com.listss.listss.Modelo.Producto;
import com.listss.listss.Persistencia.ProductosDb;
import com.listss.listss.R;

/**
 * Created by ines_ on 02/11/2017.
 */

public class NuevoProductoDialog extends DialogFragment {
    final protected DialogFragment df = this;
    protected String idLista;
    protected EditText nombre;
    protected EditText cantidad;
    protected EditText precio;
    protected ImageButton recVoz;

    public void setLista(String idLista) {
        this.idLista = idLista;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View vista = inflater.inflate(R.layout.nuevo_producto, null);
        nombre = vista.findViewById(R.id.etNombre);
        cantidad = vista.findViewById(R.id.etCantidad);
        precio = vista.findViewById(R.id.etPrecio);
        recVoz = vista.findViewById(R.id.rec_voz);
        recVoz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReconocimientoVoz.startVoiceRecognitionActivity(df);
            }
        });
        final AlertDialog ad = builder.setTitle("Nuevo producto")
                .setView(vista)
                .setPositiveButton("Añadir", null) //Set to null. We override the onclick
                .setNegativeButton(R.string.cancel, null)
                .setCancelable(false)
                .create();
        ad.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = ad.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Crear lista
                        Producto p = datosValidos(nombre, cantidad, precio);
                        if (p != null) {
                            ProductosDb.anadirProducto(idLista, p);
                            ad.dismiss();
                        }
                    }
                });
            }
        });
        return ad;
    }

    protected Producto datosValidos(EditText nombre, EditText cantidad, EditText precio) {
        String nombrev = nombre.getText().toString();
        String cantidadv = cantidad.getText().toString();
        int cv = Integer.parseInt(cantidadv);
        String preciov = precio.getText().toString();
        double pv = Double.parseDouble(preciov);
        if (nombrev.isEmpty()) {
            nombre.setError("El campo es requerido");
        } else if (nombre.length() < 3 || nombre.length() > 30) {
            nombre.setError("El nombre debe tener una longitud entre 3 y 30 caracteres");
        } else if (cantidadv.isEmpty()) {
            cantidad.setError("El campo es requerido");
        } else if (cv < 1) {
            cantidad.setError("Debe ser al menos 1");
        } else if (preciov.isEmpty()) {
            precio.setError("El campo es requerido");
        } else if (pv < 0) {
            precio.setError("El formato del precio no es adecuado");
        } else {
            Producto p = new Producto();
            p.setNombre(nombrev);
            p.setCantidad(cv);
            p.setPrecio(pv);
            return p;
        }
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String texto = ReconocimientoVoz.resultadoRV(requestCode, resultCode, data, this);
        Log.d("Resultado RV", texto);
        nombre.setText(texto);
    }
}
