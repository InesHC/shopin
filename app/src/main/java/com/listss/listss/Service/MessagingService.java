package com.listss.listss.Service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.listss.listss.Logica.Activity.InicioActivity;
import com.listss.listss.Logica.Fragment.ProductoFragment;
import com.listss.listss.Persistencia.UsuariosDb;
import com.listss.listss.R;

import java.util.Map;

/**
 * Created by ines_ on 09/11/2017.
 */

public class MessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        String lista = data.get("idLista");
        String idUsuario = data.get("idUsuario");
        if (!((ProductoFragment.isRunning && ProductoFragment.idLista.equals(lista)) || (idUsuario != null && idUsuario.equals(UsuariosDb.idUsuario())))) {
            Intent notificationIntent = new Intent(this, InicioActivity.class);
            notificationIntent.putExtra("idLista", lista);
            // Creamos el PendingIntent
            PendingIntent contentIntent = PendingIntent.getActivity(
                    this, 0, notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            ProductoFragment.idLista = lista;

            int notificationId = 1234;
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher) //a resource for your custom small icon
                    .setContentTitle(remoteMessage.getData().get("title")) //the "title" value you sent in your notification
                    .setContentText(remoteMessage.getData().get("message")) //ditto
                    .setAutoCancel(true) //dismisses the notification on click
                    .setSound(defaultSoundUri)
                    .setVibrate(new long[]{500, 500, 500, 500, 500})//Vibration
                    //.setLights(Color.GREEN, 2000, 2000)//LED
                    .setContentIntent(contentIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            try {
                notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
    }
}
