package com.listss.listss.Persistencia;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.listss.listss.General.Imagenes;
import com.listss.listss.Modelo.Lista;
import com.listss.listss.R;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.concurrent.Callable;

/**
 * Created by ines_ on 27/11/2017.
 */

public class ImagenesFirebase extends BaseDb {
    public static FirebaseStorage storage = FirebaseStorage.getInstance();
    public static StorageReference storageRef = storage.getReference();

    public static String nuevoNombreImagenLista(String idLista) {
        //Forma el nombre de la imagen con el id de la lista y la fecha actual
        return idLista + (new Date().toString()) + ".jpeg";
    }

    public static StorageReference getStorageImagenLista(String nombreImagen) {
        return storageRef.child("images").child("listas").child(nombreImagen);
    }

    public static StorageReference getImagenLista(Lista lista) {
        return getStorageImagenLista(lista.getImagen());
    }

    public static void cargarImagenLista(ImageView imageView, Lista lista) {
        if (lista.getImagen() != null) {//Comprobar si tiene imagen antes de intentar cargarla
            StorageReference image = getImagenLista(lista);
            Context context = imageView.getContext();
            GlideApp.with(context)
                    .load(image).placeholder(R.mipmap.ic_shoppingcard)
                    //.signature(new MediaStoreSignature())
                    //.diskCacheStrategy(DiskCacheStrategy.NONE)
                    //.skipMemoryCache(true)
                    .into(imageView);
        }
    }

    public static void subirImagenLista(ImageView imageView, Lista lista, final Callable accionTrasSubida) {
        if (!igualImagenDefecto(imageView)) {//Comprueba si ha cambiado la imagen por defecto
            final String idLista = lista.getId();
            //Cambia el nombre de la lista
            final String nombreNuevo = nuevoNombreImagenLista(idLista);
            StorageReference imagen = getStorageImagenLista(nombreNuevo);
            // Get the data from an ImageView as bytes
            imageView.setDrawingCacheEnabled(true);
            imageView.buildDrawingCache();
            Bitmap bitmap = imageView.getDrawingCache();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();

            UploadTask uploadTask = imagen.putBytes(data);//Sube la imagen
            eliminarImagen(lista);//Elimina la imagen anterior
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    ListasDb.cambioImagen(null, idLista);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ListasDb.cambioImagen(nombreNuevo, idLista);
                    try {
                        accionTrasSubida.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public static boolean igualImagenDefecto(ImageView imageView) {
        //Context contexto = imageView.getContext();
        //CircleImageView defecto = new CircleImageView(contexto); //Compara con imagen circular
        //defecto.setImageDrawable(contexto.getResources().getDrawable(Imagenes.imagenDefecto));
        //return imageView.getDrawable().equals(defecto.getDrawable());
        return imageView.getTag().equals(Imagenes.imagenDefecto);
    }

    public static void eliminarImagen(Lista lista) {
        if (lista.getImagen() != null) {
            StorageReference imagen = getStorageImagenLista(lista.getImagen());
            imagen.delete();
        }
    }
}
