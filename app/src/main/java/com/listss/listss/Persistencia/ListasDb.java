package com.listss.listss.Persistencia;

import android.widget.ImageView;

import com.google.firebase.database.Query;
import com.listss.listss.Modelo.Lista;

import java.util.concurrent.Callable;

/**
 * Created by ines_ on 30/10/2017.
 */

public class ListasDb extends BaseDb {

    //---- CAMBIAR DATOS DE LISTAS ----

    /**
     * Añade una lista vacía
     *
     * @param nombre
     */
    public static void nuevaLista(String nombre, ImageView imagen) {
        Lista l = new Lista(nombre);
        String idUsuario = UsuariosDb.idUsuario();
        l.addParticipante(idUsuario);
        //Crea la lista
        String idLista = listas.push().getKey();
        l.setId(idLista);
        listas.child(idLista).setValue(l);
        //Asigna la lista al usuario
        UsuariosDb.participaLista(idUsuario, idLista);
        ImagenesFirebase.subirImagenLista(imagen, l, null);
        NotificacionesDb.suscribirALista(idLista);
    }

    public static void cambioImagen(String valorCambio, String idLista) {
        listas.child(idLista).child("imagen").setValue(valorCambio);
    }

    /**
     * Cambiar nombre de una lista
     *
     * @param id
     * @param nombre
     */
    public static void setNombreLista(String id, String nombre) {
        listas.child(id).child(NOMBRE).setValue(nombre);
    }

    /**
     * Cambiar nombre e imagen de una lista
     *
     * @param lista
     * @param imagen
     */
    public static void setDatosLista(Lista lista, ImageView imagen, Callable accionTrasCambio) {
        ImagenesFirebase.subirImagenLista(imagen, lista, accionTrasCambio);
        setNombreLista(lista.getId(), lista.getNombre());
    }

    /**
     * Cambiar datos de una lista
     *
     * @param lista
     */
    public static void setLista(Lista lista) {
        listas.child(lista.getId()).setValue(lista);
    }

    public static void eliminarLista(String lista) {
        listas.child(lista).removeValue();
    }

    /**
     * Elimina un participante de una lista
     *
     * @param lista
     * @param usuario
     */
    public static void eliminarParticipante(Lista lista, String usuario) {
        if (lista.numParticipantes() == 1) {//Si es el último participante elimina la lista
            eliminarLista(lista.getId());
            ImagenesFirebase.eliminarImagen(lista);
        } else {
            listas.child(lista.getId()).child(PARTICIPANTES).child(usuario).removeValue();
        }
        NotificacionesDb.dessuscribirALista(lista.getId());
        UsuariosDb.eliminarLista(usuario, lista.getId());
    }

    /**
     * Añade un nuevo participante a una lista
     *
     * @param id
     * @param usuario
     */
    public static void anadirParticipante(String id, String usuario) {
        listas.child(id).child(PARTICIPANTES).child(usuario).setValue(true);
        NotificacionesDb.suscribirALista(id);
        UsuariosDb.participaLista(usuario, id);
    }

    /**
     * Obtiene los participantes de la lista
     *
     * @param id
     */
    public static Query getParticipantes(String id) {
        return listas.child(id).child(PARTICIPANTES);
    }


    //---- OBTENER DATOS DE LISTAS ----

    public static Query getLista(String id) {
        return listas.child(id);
    }
}
