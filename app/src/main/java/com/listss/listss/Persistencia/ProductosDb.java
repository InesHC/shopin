package com.listss.listss.Persistencia;

import android.content.Context;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.listss.listss.Modelo.Lista;
import com.listss.listss.Modelo.Producto;

/**
 * Created by ines_ on 02/11/2017.
 */

public class ProductosDb extends BaseDb {
    /**
     * Referencia a los productos de una lista
     *
     * @param idlista
     * @return
     */
    public static DatabaseReference refProductos(String idlista) {
        return listas.child(idlista).child(PRODUCTOS);
    }

    /**
     * Devuelve la lista de productos ordenada por el nombre de los mismos
     *
     * @param idLista
     * @return
     */
    public static Query getProductos(String idLista) {
        return refProductos(idLista).orderByChild("comprado");
    }

    //---- CAMBIAR DATOS DE PRODUCTOS ----

    /**
     * Añade un producto a una lista
     *
     * @param lista
     * @param producto
     */
    public static void anadirProducto(String lista, Producto producto) {
        refProductos(lista).push().setValue(producto);
    }

    /**
     * Elimina un producto de una lista
     *
     * @param id
     * @param idproducto
     */
    public static void eliminarProducto(String id, String idproducto) {
        refProductos(id).child(idproducto).removeValue();
    }

    /**
     * Cambia los datos de un producto de una lista
     *
     * @param idlista
     * @param id
     * @param producto
     */
    public static void setProducto(String idlista, String id, Producto producto) {
        refProductos(idlista).child(id).setValue(producto);
    }

    /**
     * Indica que ha comprado un producto de una lista
     *
     * @param lista
     * @param id
     */
    public static void comprar(Lista lista, String id, boolean comprado, Context contexto) {
        refProductos(lista.getId()).child(id).child("comprado").setValue(comprado);
        //Mandar notificación al resto de participantes
        //NotificacionesDb.suscribirALista(lista.getId());//<--- ELIMINAR!!
        if (comprado) {
            NotificacionesDb.addNotificationKey(lista, contexto);
        }
    }

}
