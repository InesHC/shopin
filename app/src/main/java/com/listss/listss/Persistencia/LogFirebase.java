package com.listss.listss.Persistencia;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.listss.listss.Logica.Activity.LoginActivity;
import com.listss.listss.Logica.Activity.RegistroActivity;
import com.listss.listss.R;

import java.util.concurrent.Callable;

/**
 * Created by ines_ on 30/10/2017.
 */

public class LogFirebase extends BaseDb {
    private static String TAG="Usuario";

    public static void crearUsuario(String email, String pwd, final RegistroActivity actividad){
        mAuth.createUserWithEmailAndPassword(email, pwd)
                .addOnCompleteListener(actividad, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            actividad.updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(actividad, "Error de autenticación.",
                                    Toast.LENGTH_SHORT).show();
                            actividad.updateUI(null);
                        }

                        // ...
                    }
                });
    }
    public static void iniciarSesion(String email, String pwd, final LoginActivity actividad){
        mAuth.signInWithEmailAndPassword(email, pwd)
                .addOnCompleteListener(actividad, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            actividad.updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(actividad, "Error de autenticación.",
                                    Toast.LENGTH_SHORT).show();
                            actividad.updateUI(null);
                        }

                        // ...
                    }
                });
    }
    public static void iniciarConGoogle(AuthCredential credential, final LoginActivity actividad){
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(actividad, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            actividad.updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(actividad, "Error de autenticación.",
                                    Toast.LENGTH_SHORT).show();
                            actividad.updateUI(null);
                        }

                        // ...
                    }
                });
    }
    public static void cerrarSesion(AppCompatActivity actividad, final Callable updateUI){
        mAuth.signOut();

        // Google sign out
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(actividad.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleSignInClient gsic = GoogleSignIn.getClient(actividad,gso);

        gsic.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                try{updateUI.call();}
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
    public static GoogleApiClient getGoogleApiClient(AppCompatActivity activity){
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(activity.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(activity)
                .enableAutoManage(activity /* FragmentActivity */, null /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        return googleApiClient;
    }
    public void nuevoUsuario(String nombre, String pwd){
        usuarios.setValue(nombre);
    }
}
