package com.listss.listss.Persistencia;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessaging;
import com.listss.listss.Modelo.Lista;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ines_ on 29/11/2017.
 */

public class NotificacionesDb {
    public static FirebaseMessaging mensajes = FirebaseMessaging.getInstance();

    public static void suscribirALista(String idLista) {
        mensajes.subscribeToTopic(idLista);
    }

    public static void dessuscribirALista(String idLista) {
        mensajes.unsubscribeFromTopic(idLista);
    }

    public static void addNotificationKey(final Lista lista, Context contexto) {
        RequestQueue queue = Volley.newRequestQueue(contexto);
        String url = "https://fcm.googleapis.com/fcm/send";
        try {
            JSONObject params = new JSONObject();
            params.put("to", "/topics/" + lista.getId());
            params.put("message", "Compra");
            JSONObject data = new JSONObject();
            data.put("message", "La lista está comprándose");
            data.put("title", lista.getNombre());
            data.put("idLista", lista.getId());
            data.put("idUsuario", UsuariosDb.idUsuario()); //Envía el usuario que inicia la notificación
            params.put("data", data);
            JsonObjectRequest postRequest = new JsonObjectRequest(url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // response
                            Log.d("Response", response.toString());
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", error.getMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "key=AIzaSyAaw3eqmB-rwWqj9rsGzaRWH6ZqG88a7xw");
                    return headers;
                }
            };
            queue.add(postRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
