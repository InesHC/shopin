package com.listss.listss.Persistencia;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.Query;
import com.listss.listss.Modelo.Usuario;

/**
 * Created by ines_ on 30/10/2017.
 */

public class UsuariosDb extends BaseDb {
    public static FirebaseUser estadoUsuario(){
        return mAuth.getCurrentUser();
    }

    public static String idUsuario(){
        return estadoUsuario().getUid();
    }

    public static Query listasUsuario(){
        return usuarios.child(idUsuario()).child(LISTAS);
    }

    public static void nuevoUsuario(String id, String nombre){
        Usuario usuario=new Usuario();
        BaseDb.usuarios.child(id).setValue(usuario);
    }

    public static void participaLista(String usuario, String idLista){
        usuarios.child(usuario).child(LISTAS).child(idLista).setValue(true);
    }

    public static void eliminarLista(String usuario, String idLista){
        usuarios.child(usuario).child(LISTAS).child(idLista).removeValue();
    }
}
