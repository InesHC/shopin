package com.listss.listss.Persistencia;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by ines_ on 30/10/2017.
 */

public class BaseDb {
    protected static String TAGLOG="Log BD";
    protected final static String LISTAS="listas";
    protected final static String USUARIOS="usuarios";
    protected final static String PARTICIPANTES="participantes";
    protected final static String NOMBRE="nombre";
    protected final static String PRODUCTOS="productos";
    protected final static FirebaseDatabase db=db();
    public static void iniciarConfiguracion(){
        listas.keepSynced(true);
        usuarios.keepSynced(true);
    }
    protected static FirebaseDatabase db(){
        if (db == null) {
            FirebaseDatabase dbaux = FirebaseDatabase.getInstance();
            dbaux.setPersistenceEnabled(true);
            return dbaux;
        }
        return db;
    }

    protected static DatabaseReference listas=db.getReference(LISTAS);
    protected static DatabaseReference usuarios=db.getReference(USUARIOS);
    protected static FirebaseAuth mAuth=FirebaseAuth.getInstance();
}
