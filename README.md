# ShopIn

This Android APP could help you to create and share shopping lists with other users.

## Installation
Clone this repository and import into **Android Studio**
```bash
git clone https://InesHC@bitbucket.org/InesHC/shopin.git
```

## Generating signed APK
From Android Studio:

1. ***Build*** menu
2. ***Generate Signed APK...***
3. Fill in the keystore information *(you only need to do this once manually and then let AndroidStudio remember it)*

## Screenshots

![Login](/screenshots/inicio_sesion.png)
![Home options](/screenshots/opciones_inicio.png)
![Lists](/screenshots/varias_listas.png)
![List options](/screenshots/opciones_lista.png)
![Share list](/screenshots/compartir_lista.png)
![New product](/screenshots/nuevo_producto.png)

## Author
This project was created by [Inés Hernández](https://bitbucket.org/InesHC)

## Maintainers
This project is mantained by [Inés Hernández](https://bitbucket.org/InesHC)
